import React from 'react';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';

import '../style/calender.css';

interface IAktivitet {
    BokNr: string;
    BokadAv: string;
    Bokningsgrupp: string;
    FrNKl: string;
    Frandatum: Date;
    Plats: string;
    PrelBokad: string;
    Tidstext: string;
    TillKl: string;
    Tilldatum: Date;
    Titel: string;
    Utskriven: string;
    _id: string;
};

type Props = {
    dateArray: {
        day: string,
        activties: IAktivitet[],
        currentMonth: boolean,
    };
};

type State = {
    open: boolean;
    selectedActivtie: number;
};

class DateDays extends React.Component<Props, State> {
    state: State = {
        open: false,
        selectedActivtie: 0,
    };

    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    handleSelectedActivtie = (event: React.MouseEvent<HTMLElement>, id: number) => {
        this.setState({ selectedActivtie: id });
    };

    render() {
        const activtie: IAktivitet[] = this.props.dateArray.activties;
        const activtieNumber: number = this.state.selectedActivtie;
        let showActivtie: boolean = false;

        if (this.props.dateArray.activties.length > 0) {
            showActivtie = true;
        }

        return (
            <div>
                <Paper className="date-day-paper" style={{ background: this.props.dateArray.currentMonth ? "white" : "lightgrey" }} elevation={2} onClick={this.handleClickOpen}>
                    <div className="date-day-div">
                        <span className="date-span">{this.props.dateArray.day}</span>
                        <div className="date-activites">
                            {this.props.dateArray.activties.slice(0, 3).map((data: IAktivitet, id: number) => (
                                <div key={id}>
                                    <span className="time-span">{data.FrNKl}: </span>
                                    <span className="boknr-span">{data.BokNr}</span>
                                </div>
                            ))}
                        </div>
                    </div>
                </Paper>

                <Dialog onClose={this.handleClose} aria-labelledby="customized-dialog-title" open={this.state.open}>
                    {showActivtie &&
                        <Grid className="dialog-grid" container>
                            <Grid item xs={6}>
                                <div>
                                    <h3 className="dialog-title">{activtie[activtieNumber].Titel}</h3>
                                    <Divider />
                                    <div className="activtie-full-show">
                                        <h4>{activtie[activtieNumber].Bokningsgrupp}</h4>
                                        <Divider />
                                        <p className="info-span">Reservation Number: </p>
                                        <span>{activtie[activtieNumber].BokNr} </span>
                                        <Divider />
                                        <div>
                                            <p className="info-span">Time: </p>
                                            <span className="time-span">
                                                {activtie[activtieNumber].FrNKl}
                                            </span>
                                            <span> - </span>
                                            <span className="time-span">
                                                {activtie[activtieNumber].TillKl}
                                            </span>
                                            <Divider />
                                        </div>
                                    </div>
                                </div>
                            </Grid>

                            <Grid item xs={6} className="grid-list-actvite">
                                <h3 className="dialog-title">Activties</h3>
                                <Divider />
                                <List className="list-activties">
                                    {this.props.dateArray.activties.map((data: IAktivitet, id: number) => (
                                        <ListItem key={id}>
                                            <div key={id} onClick={(e) => { this.handleSelectedActivtie(e, id) }}>
                                                <Button>
                                                    <span>{data.Titel}: </span>
                                                    <span className="time-span"> {data.FrNKl}</span>
                                                </Button>
                                                <Divider />
                                            </div>
                                        </ListItem>
                                    ))}
                                </List>
                            </Grid>
                        </Grid>
                    }
                    {!showActivtie &&

                        <div className="no-activites">
                            <h3>No Current Activties</h3>
                            <Divider />
                        </div>
                    }
                    <Button variant="contained" color="primary" autoFocus onClick={this.handleClose}>
                        Close
                    </Button>
                </Dialog>
            </div>
        );
    }
}

export default DateDays;