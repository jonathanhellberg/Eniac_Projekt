import { useQuery } from "@apollo/client";
import gql from "graphql-tag";
import React, { useState, useEffect } from "react";
import moment from "moment";

import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import DateDays from './components/DateDays';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import './style/calender.css';

interface IAktivitet {
  BokNr: string;
  BokadAv: string;
  Bokningsgrupp: string;
  FrNKl: string;
  Frandatum: Date;
  Plats: string;
  PrelBokad: string;
  Tidstext: string;
  TillKl: string;
  Tilldatum: Date;
  Titel: string;
  Utskriven: string;
  _id: string;
}

interface ICalDays {
  day: string;
  activties: IAktivitet[];
  currentMonth: boolean;
}

type CalenderDays = ICalDays[];

const Kalender = () => {

  const startYear: number = 2017;
  const lastYear: number = 1930;

  const startMonth: string = "November";

  const startDate: string = "2017-11-01T00:00:00Z";
  const endDate: string = "2017-11-30T00:00:00Z";

  const [month, setMonth] = useState(startMonth);
  const [year, setYear] = useState(startYear);
  const [franDatum, setFranDatum] = useState<string>(startDate);
  const [tillDatum, setTillDatum] = useState<string>(endDate);

  const [calender, setCalender] = useState<CalenderDays[]>([]);

  const getMonths: string[] = moment.months();
  const getDays: number = moment(year + "-" + month, "YYYY-MMMM").daysInMonth();

  moment.updateLocale('en', {
    week: {
      dow: 1,
    },
  })

  const getWeekDays: string[] = moment.weekdays(true);

  const { loading, error, data } = useQuery(
    gql`
      query aktiviteter($franDatum: DateTime, $tillDatum: DateTime) {
        aktivitets(
          query: { Frandatum_gte: $franDatum, Frandatum_lte: $tillDatum }
        ) {
          BokNr
          BokadAv
          Bokningsgrupp
          FrNKl
          Frandatum
          Plats
          PrelBokad
          Tidstext
          TillKl
          Tilldatum
          Titel
          Utskriven
          _id
        }
      }
    `,
    {
      variables: {
        franDatum: franDatum,
        tillDatum: tillDatum,
      },
    }
  );

  const getYears = (endYear: number) => {
    const currentYear: number = moment().year();
    let years: string[] = [];

    for (let year: number = endYear; year < (currentYear + 1); year++) {
      years.push(year.toString());
    }

    years.sort().reverse();

    return years;
  };

  const getPreviousMonthDays = () => {

    const previousMonthOfYear = moment(year + "-" + month, "YYYY-MMMM").subtract(1, 'months');

    const monthLastMonday: number = parseInt(previousMonthOfYear.endOf('month').startOf('isoWeek').format("DD"));
    const lastDayOfMonth: number = parseInt(previousMonthOfYear.endOf('month').format('DD'));
    const getFirstDayOfMonth: string = moment(year + "-" + month, "YYYY-MMMM").startOf('month').format('dddd');

    const lastDaysOfMonthArray: CalenderDays = [];

    const previousMonth: string = moment(month, "MMMM").subtract(1, 'months').format("MMMM");

    if (getFirstDayOfMonth !== "Monday") {
      for (let day: number = monthLastMonday; day < (lastDayOfMonth + 1); day++) {

        let formatDay: string = moment(year + "-" + previousMonth + "-" + day, "YYYY-MMMM-D").format("DD");

        lastDaysOfMonthArray.push({ day: formatDay, activties: [], currentMonth: false });
      }
    }

    return lastDaysOfMonthArray;
  };

  const getUpComingMonthDays = () => {

    const upComingMonthOfYear = moment(year + "-" + month, "YYYY-MMMM").add(1, 'months');

    const firstDayAfterMonthEnd: number = parseInt(upComingMonthOfYear.startOf('month').format('DD'));
    const lastDayOfWeekNextMonth: number = parseInt(upComingMonthOfYear.startOf('month').endOf('week').format("DD"));
    const getLastDayInMonth: string = moment(year + "-" + month, "YYYY-MMMM").endOf('month').format('dddd')

    const firstDaysOfMonthArray: CalenderDays = [];

    const upComingMonth: string = moment(year + "-" + month, "YYYY-MMMM").add(1, 'months').format("MMMM");

    if (getLastDayInMonth !== "Sunday") {
      for (let day: number = firstDayAfterMonthEnd; day < (lastDayOfWeekNextMonth + 1); day++) {

        let formatDay: string = moment(year + "-" + upComingMonth + "-" + day, "YYYY-MMMM-D").format("DD");

        firstDaysOfMonthArray.push({ day: formatDay, activties: [], currentMonth: false });
      }
    }

    return firstDaysOfMonthArray;
  };

  const getWeek = () => {

    const upComingMonthSecondWeek = moment(year + "-" + month, "YYYY-MMMM").add(1, 'months').add(1, 'weeks');

    const firstDayOfWeek: number = parseInt(upComingMonthSecondWeek.startOf('month').format('DD'));
    const upComingMonth: string = moment(year + "-" + month, "YYYY-MMMM").add(1, 'months').add(1, 'weeks').format("MMMM");

    const days: CalenderDays = [];
    const week = 7;

    for (let day: number = firstDayOfWeek; day < (week + 1); day++) {

      const formatDay: string = moment(year + "-" + upComingMonth + "-" + day, "YYYY-MMMM-D").format("DD");

      days.push({ day: formatDay, activties: [], currentMonth: false });
    }

    return days;
  };

  const getDay = () => {

    let days: CalenderDays = [];

    for (let day: number = 0; day < getDays; day++) {
      const dayFormat: string = moment(year + "-" + month).add(day, 'days').format('DD');

      days[day] = { day: dayFormat, activties: [], currentMonth: true };
    }

    return days;
  };

  const getActivities = (days: CalenderDays) => {

    for (let day: number = 0; day < getDays; day++) {
      const dayFormat: string = moment(year + "-" + month).add(day, 'days').format('DD');
      const dateFormat: string = moment(year + "-" + month + "-" + dayFormat, "YYYY-MMMM-DD").format("YYYY-MM-DDTHH:mm:ss") + "Z";

      for (const activity of data.aktivitets) {
        if (activity.Frandatum === dateFormat) {
          days[day].activties.push(activity);
        }
      }
    }

    return days;
  };

  useEffect(() => {

    const lastMonths: CalenderDays = getPreviousMonthDays();
    const firstMonths: CalenderDays = getUpComingMonthDays();
    const spliceDaysToWeeks: CalenderDays[] = [];

    const fiveWeeks = 35;
    const week = 7;

    let calenderDays: CalenderDays = [];

    calenderDays = getDay();

    if (data && !loading) {
      calenderDays = getActivities(calenderDays);
    }

    calenderDays = lastMonths.concat(calenderDays);
    calenderDays = calenderDays.concat(firstMonths);

    if (calenderDays.length === fiveWeeks) {
      calenderDays = calenderDays.concat(getWeek());
    }

    while (calenderDays.length) {
      spliceDaysToWeeks.push(calenderDays.splice(0, week));
    }

    setCalender(spliceDaysToWeeks);

  }, [franDatum, data]);

  const handleChangeMonth = (event: any) => {

    const currentMonth: string = event.target.value;
    const lastDayInMonth: number = moment(year + "-" + currentMonth, "YYYY-MMMM").daysInMonth();

    const franDatum: string = moment(year + "-" + currentMonth + "-01", "YYYY-MMMM-DD").format("YYYY-MM-DDTHH:mm:ss") + "Z";
    const tillDatum: string = moment(year + "-" + currentMonth + "-" + lastDayInMonth, "YYYY-MMMM-DD").format("YYYY-MM-DDTHH:mm:ss") + "Z";

    setMonth(currentMonth);

    setFranDatum(franDatum);
    setTillDatum(tillDatum);
  };

  const handleChangeYear = (event: any) => {

    const currentYear: number = event.target.value;
    const lastDayInMonth: number = moment(currentYear + "-" + month, "YYYY-MMMM").daysInMonth();

    const franDatum: string = moment(currentYear + "-" + month + "-01", "YYYY-MMMM-DD").format("YYYY-MM-DDTHH:mm:ss") + "Z";
    const tillDatum: string = moment(currentYear + "-" + month + "-" + lastDayInMonth, "YYYY-MMMM-DD").format("YYYY-MM-DDTHH:mm:ss") + "Z";

    setYear(currentYear);

    setFranDatum(franDatum);
    setTillDatum(tillDatum);
  };

  return (
    <Container>
      {/* ev. felmeddelande */}
      {error?.message}
      {/* om det finns data så visar vi alla aktiviteter som hämtats */}

      <Grid container>
        <Grid item xs={8}>
          <h1>Kalender</h1>
        </Grid>

        <Grid item xs={4} >

          <FormControl className='form-month'>
            <Select
              value={month}
              onChange={handleChangeMonth}
              className='select-month'
            >
              {getMonths.map((month: string, id: number) => (
                <MenuItem key={id} value={month}>{month}</MenuItem>
              ))}
            </Select>
          </FormControl>

          <FormControl className='form-year'>
            <Select
              value={year}
              onChange={handleChangeYear}
              className='select-year'
            >
              {getYears(lastYear).map((year: string, id: number) => (
                <MenuItem key={id} value={year}>{year}</MenuItem>
              ))}
            </Select>
          </FormControl>

        </Grid>
      </Grid>

      <Grid container>
        <table className="calender-table">
          <thead className="weekdays">
            <tr>
              {getWeekDays.map((weekdays: string) => (
                <th className="weekdays">{weekdays}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {
              calender.map((weeks: CalenderDays) => (
                <tr>
                  {weeks.map((day: ICalDays) => (
                    <td className="calender-table-column">
                      <DateDays dateArray={day} />
                    </td>
                  ))}
                </tr>
              ))}
          </tbody>
        </table>
      </Grid>
    </Container>
  );
};

export default Kalender;
